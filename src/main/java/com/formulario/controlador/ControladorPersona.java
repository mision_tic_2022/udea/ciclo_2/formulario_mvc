package com.formulario.controlador;

import com.formulario.modelo.Persona;

public class ControladorPersona {
    //ATRIBUTOS
    private Persona[] personas;

    //CONSTRUCTOR
    public ControladorPersona(){
        personas = new Persona[10];
    }

    public Persona getPersona(int pos){
        return personas[pos];
    }

    public int lengthPersonas(){
        return personas.length;
    }

    public void setPersona(Persona persona, int pos){
        this.personas[pos] = persona;
    }

    public void crearPersona(String nombre, String apellido, int edad, String cedula){
        Persona persona = new Persona(nombre, apellido, edad, cedula);
        int i = 0;
        do{
            if(getPersona(i) == null){
                setPersona(persona, i);
                break;
            }
            i++;
        }while(i < lengthPersonas());

        System.out.println("Persona registrada con exito");
    }
}
