package com.formulario.vista;

import javax.swing.JOptionPane;

import com.formulario.controlador.ControladorPersona;

public class GuiPersona {

    // ATRIBUTO
    ControladorPersona controladorPersona;

    // CONSTRUCTOR
    public GuiPersona() {
        controladorPersona = new ControladorPersona();
    }

    public void crear_persona(){
        try{
            //SOLICITAR DATOS DE LA PERSONA
            String nombre = JOptionPane.showInputDialog("Por favor ingrese el nombre");
            String apellido = JOptionPane.showInputDialog("Por favor ingrese el apellido");
            int edad = Integer.parseInt( JOptionPane.showInputDialog("Por favor ingrese la edad") );
            String cedula = JOptionPane.showInputDialog("Por favor ingrese la cédula");
            //Crear una persona por medio del controlador
            controladorPersona.crearPersona(nombre, apellido, edad, cedula);
        }catch(Exception error){
            System.out.println(error.getMessage());
            JOptionPane.showMessageDialog(null, "Datos incorrectos");
        }
        
    }

    public void mostrar_personas(){
        String infoPersonas = "-------------------INFORMACIÓN DEL PERSONAL-------------------------\n";
        for(int i = 0; i < controladorPersona.lengthPersonas(); i++){
            if(controladorPersona.getPersona(i) != null){
                infoPersonas += controladorPersona.getPersona(i);
                infoPersonas += "\n";
            }
        }
        //Mostrar información
        JOptionPane.showMessageDialog(null, infoPersonas);

    }

    public void menu() {

        String mensaje = "--------------------FORMULARIO--------------------\n";
        mensaje += "1) Crear persona\n";
        mensaje += "2) Mostrar personas\n";
        mensaje += "-1) Salir\n";
        int opcion = 0;
        do {
            try {
                // Mostrar y capturar datos usando la Clase JoptionPane
                opcion = Integer.parseInt(JOptionPane.showInputDialog(mensaje));
                //Evaluar la opción capturada
                switch(opcion){
                    case 1:
                        crear_persona();
                        break;
                    case 2:
                        mostrar_personas();
                    break;
                    case -1:
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Por favor ingrese una opción válida");
                }
            } catch (Exception error) {
                System.out.println(error.getMessage());
                JOptionPane.showMessageDialog(null, "Debe ingresar una opción numérica");
            }
        } while (opcion != -1);

    }

}

/***
 * Castear/convertir a otro tipo de dato
 * Integer.parseInt("2")
 * 
 * //Mostrar info
 * JOptionPane.showMessageDialog(null, "La opción ingresada es : "+opcion);
 */
