package com.formulario.vista;

import java.util.Scanner;

import com.formulario.controlador.ControladorPersona;

public class VistaPersona {
    // ATRIBUTOS
    private ControladorPersona controladorPersona;

    // CONSTRUCTOR
    public VistaPersona() {
        controladorPersona = new ControladorPersona();
    }

    public void mostrar_personas() {
        for (int i = 0; i < controladorPersona.lengthPersonas(); i++) {
            if (controladorPersona.getPersona(i) != null) {
                System.out.println(controladorPersona.getPersona(i));
            }
        }
    }

    public void crear_persona(Scanner leer) {
        System.out.println("Ingrese el nombre: ");
        String nombre = leer.next();

        System.out.println("Ingrese el apellido: ");
        String apellido = leer.next();

        System.out.println("Ingrese la edad: ");
        int edad = leer.nextInt();

        System.out.println("Ingrese la cedula: ");
        String cedula = leer.next();

        controladorPersona.crearPersona(nombre, apellido, edad, cedula);
    }

    public void menu() {
        String mensaje = "-----------------------FORMULARIO-----------------------\n";
        mensaje += "1) Crear persona\n";
        mensaje += "2) Mostrar personas\n";
        mensaje += "-1) Salir\n";
        mensaje += ">>> ";
        try (Scanner leer = new Scanner(System.in)) {
            int opcion = 0;
            do {

                System.out.print(mensaje);
                //Capturar la opción por consola
                opcion = leer.nextInt();
                leer.nextLine();

                //Evaluar la opción ingresada
                switch(opcion){
                    case 1:
                        crear_persona(leer);
                        break;
                    case 2:
                        mostrar_personas();
                        break;
                    case -1:
                        break;
                    default:
                        System.out.println("Por favor ingrese una opcion valida");
                }

            } while (opcion != -1);

        } catch (Exception error) {

        }
    }

}
